public class GGT {
	public static void main(String[] args) {
		//Prüfe, ob a und b \in N
		if (Integer.parseInt(args[0]) <= 0 || Integer.parseInt(args[1]) <= 0) {
			System.out.println("nur positive ganze Zahlen als Argumente erlaubt");
			System.exit(-1);
		}
		
		int a = Integer.parseInt(args[0]);
		int b = Integer.parseInt(args[1]);
		
		//1.
		int m = a;
		int n = b;
		
		int r = 1;
		
		//5.
		while (r!=0) {
			//2.
			if (m < n) {
				int tmp = m;
				m = n;
				n = tmp;
			}
			
			//3.
			r = m - n;
			
			//4.
			m = n;
			n = r;
		}
		
		//Gebe das Ergebnis aus
		System.out.println("ggT(" + a + ", " + b + ") = " + m);
	}
}
