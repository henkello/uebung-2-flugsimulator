
public class FlugSimulator {
	public static void main(String[] args) {
		int seats = 75;
		int max = 78;
		int tries = Integer.parseInt(args[0]);
		float chance = 0.92f;
		
		int fails = 0;
		int total = 0;
		for(int j = 0; j < tries; j++) {
			int came = 0;
			for (int i = 0; i < max; i++) {
				if (Math.random()<chance) {
					came++;
				}
			}
			if (came > seats) {
				fails++;
			}
			total += came;
		}
		System.out.println("Überbuchungen: " + fails + " (" + round(fails/(float)tries*100, 2) + "%)");
		System.out.println("Mittelwert: " + round(total/(float)tries, 1));
	}
	
	public static String round(double d, int prec) {
		double factor = Math.pow(10, prec);
		return "" + Math.round(d*factor)/factor;
	}
}